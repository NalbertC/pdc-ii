CREATE SCHEMA bd_produtos;

USE bd_produtos;

CREATE TABLE produto (
  id INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(20) NOT NULL,
  descricao VARCHAR(50) NOT NULL,
  fornecedor VARCHAR(20) NOT NULL,
  preco DECIMAL NOT NULL,
  PRIMARY KEY (id));